Установка
=========
php artisan migrate
php artisan db:seed


Сategory
========
- truck (грузовик)
- car (легковой автомобиль)
- buy (покупка)
- repair (ремонт)
- spare part (запасная часть)
- foreign (иномарка)
- home (отечественные)


Разработка
==========

php artisan semantic:tools:lemma


Тестирование
============
php vendor/bin/phpunit tests/Component/Ftp/ClientTest.php

-- Маперр
- php vendor/bin/phpunit tests/Component/Content/Mapper223Test.php

-- Токенайзер


php artisan parser:read:files

Тест для проверки семантического анализатора
============================================
php vendor/bin/phpunit tests/Category/ControlTest.php

INSERT INTO catalog_word (id, is_active, lemma) VALUES (801, 1, 'xcmg');
INSERT INTO catalog_word (id, is_active, lemma) VALUES (802, 1, 'l34');
INSERT INTO catalog_word (id, is_active, lemma) VALUES (803, 1, 'готтвальд');
INSERT INTO catalog_word (id, is_active, lemma) VALUES (804, 1, 'кран');
INSERT INTO catalog_word (id, is_active, lemma) VALUES (805, 1, 'сменнозапасной');
INSERT INTO catalog_word (id, is_active, lemma) VALUES (806, 1, 'кондор');
INSERT INTO catalog_word (id, is_active, lemma) VALUES (807, 1, 'альбатрос');
INSERT INTO catalog_word (id, is_active, lemma) VALUES (808, 1, 'сокол');
INSERT INTO catalog_word (id, is_active, lemma) VALUES (809, 1, 'зч');

INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (801, 1, 10);
INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (801, 6, 10);
INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (802, 1, 10);
INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (802, 6, 10);
INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (805, 5, 10);
INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (806, 7, 10);
INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (807, 7, 10);
INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (808, 7, 10);
INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (809, 5, 10);

INSERT INTO catalog_word (id, is_active, lemma) VALUES (810, 1, 'отечественый');
INSERT INTO catalog_word (id, is_active, lemma) VALUES (811, 1, 'тракторной');

INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (810, 7, 10);
INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (811, 7, 10);