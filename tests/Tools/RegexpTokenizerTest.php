<?php
namespace Tests\Stem\Semantic;

use App\Component\Tools\RegexpTokenizer;
use Tests\TestCase;

class RegexpTokenizerTest extends TestCase
{
    /**
     * @var RegexpTokenizer
     */
    private $service;

    public function setUp()
    {
        $this->service = new RegexpTokenizer();
    }

    /**
     * @dataProvider additionProvider
     */
    public function testList($count, $text)
    {
        $listWords = $this->service->tokenize($text);

        $this->assertEquals($count, sizeof($listWords));
    }

    public function additionProvider()
    {
        return [
            [7, "Бампер задний для Toyota Corolla E15 2006-2013"],
            [10, "Накладка переднего,бампера под номер.для Toyota Camry V50 2011>"],
            [8, "Кронштейн двигателя передний для Toyota Carina E 1992-1997"],
        ];
    }
}


