<?php
namespace Tests\Component\Content;

use Tests\TestCase;

class CacheTest extends TestCase
{
    public function testWork()
    {
        $text = 'I work!';

        \Illuminate\Support\Facades\Cache::store('file')->put('test', $text, 120);

        $value = \Illuminate\Support\Facades\Cache::store('file')->get('test');

        $this->assertEquals($text, $value);
    }
}