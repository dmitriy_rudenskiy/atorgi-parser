<?php
namespace Tests\Component\Content;

use Tests\TestCase;

class GetCategoryTest extends TestCase
{
    public function testCommand()
    {
        $text = 'Право заключения договора на поставку запасных частей для автомобилей марки ГАЗ для нужд ПАО «МРСК Центра и Приволжья» филиала «Нижновэнерго» (51305)';
        $dir = base_path();
        $command = sprintf("cd %s; php artisan semantic:get:category '%s'", $dir, $text);

        $result = exec($command);

        $this->assertEquals('Автозапчасть на легковой', $result);
    }
}