<?php
namespace Tests;

use Laravel\Lumen\Testing\TestCase as Base;

class TestCase extends Base
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }
}
