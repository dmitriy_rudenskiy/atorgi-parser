<?php
namespace Tests\Component\Ftp;

use App\Component\Ftp\Type223\Client as Client223;
use App\Component\Ftp\Type44\Client as Client44;

use Tests\TestCase;

class GroupTest extends TestCase
{
    public function testConnect44()
    {
        $ftp = new Client44();
        $listFiles = $ftp->getRoot();

        $this->assertNotEmpty($listFiles);
        $this->assertEquals('/94fz', $listFiles[0]);
    }

    public function testConnect223()
    {
        $ftp = new Client223();
        $listFiles = $ftp->getRoot();

        $this->assertNotEmpty($listFiles);
        $this->assertEquals('/out', $listFiles[0]);
    }
}