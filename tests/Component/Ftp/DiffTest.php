<?php
namespace Tests\Component\Ftp;

use App\Component\Ftp\Download\Diff;
use App\Component\Ftp\Type223\Client as Client223;
use App\Component\Ftp\Type44\Client as Client44;
use App\Entities\Catalog\Region;
use Tests\TestCase;

class DiffTest extends TestCase
{
    public function testConnect44()
    {
        $cities = Region::where('type', '44')->get();
        $ftp = new Client44();
        $diff = new Diff();

        //$diff->getListFiles($ftp, $cities);
    }

    public function testConnect223()
    {
        $cities = Region::where('type', '223')->get();
        $ftp = new Client223();
        $diff = new Diff();

        //$diff->getListFiles($ftp, $cities);
    }
}