<?php
namespace Tests\Component\Content;

use App\Component\Ftp\Type223\Mapper;
use Tests\TestCase;

class GroupTest extends TestCase
{
    public function testReadFile()
    {
        $filename = realpath(__DIR__ . '/src/223/purchaseNotice_Adygeya_Resp_20150224_000000_20150224_235959_daily_001.xml');
        $mapper = new Mapper();
        $status = $mapper->read($filename);

        $this->assertEquals(Mapper::READ_FILE, $status);
    }

    public function testSeparateContent()
    {
        $filename = realpath(__DIR__ . '/src/223/purchaseNotice_Adygeya_Resp_20150224_000000_20150224_235959_daily_001.xml');
        $mapper = new Mapper();
        $status = $mapper->read($filename);

        $this->assertEquals(Mapper::READ_FILE, $status);
        $this->assertEquals(4, sizeof($mapper->getFiles()));
        $this->assertEquals("2015-02-24T08:31:45", date("Y-m-d\TH:i:s", $mapper->getFiles()[0]["create"]));
        $this->assertEquals("ОАО ГАЗПРОМ ГАЗОРАСПРЕДЕЛЕНИЕ МАЙКОП", $mapper->getNameCompany());
        $this->assertEquals("31502057838", $mapper->getRegistrationNumber());
        $this->assertEquals("Инженерно-геодезические изыскания", $mapper->getName());
        $this->assertEquals("инженерно-геодезические изыскания", $mapper->getDescription());

        //var_dump($mapper->get()["lots"]);
        //var_dump(array_keys($mapper->get()), $mapper->get()["customer"]["mainInfo"]["shortName"]);
        //var_dump($mapper->getNameCompany());
    }
}