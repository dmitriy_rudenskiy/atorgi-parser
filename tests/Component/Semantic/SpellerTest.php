<?php
namespace Tests\Stem\Semantic;

use App\Component\Semantic\Speller;
use Tests\TestCase;

class SpellerTest extends TestCase
{
    /**
     * @var Speller
     */
    private $service;

    public function setUp()
    {
        $this->service = new Speller();
    }

    public function testSimple()
    {
        $text = "на поставку запасных частей к транспортнымсредствам марки «Скания» для выполнения работ по техническому обслуживанию иремонту";
        $result = $this->service->check($text);

        $this->assertNotEmpty($result);
        $this->assertEquals(3, sizeof($result));
        $this->assertEquals(last($result), "ремонту");
    }
}


