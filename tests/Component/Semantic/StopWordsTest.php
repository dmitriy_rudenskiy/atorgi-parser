<?php
namespace Tests\Stem\Semantic;

use App\Component\Semantic\Stem;
use App\Component\Semantic\StopWords;
use Tests\TestCase;

class StopWordsTest extends TestCase
{
    /**
     * @var StopWords
     */
    private $service;

    public function setUp()
    {
        $this->service = new StopWords();
    }

    public function testSimple()
    {
        $text = "на поставку запасных частей к транспортнымсредствам марки «Скания» для выполнения работ по техническому обслуживанию иремонту";
        $stem = new Stem();
        $array = $stem->getListWord($text);
        $result = $this->service->clear($array);

        $this->assertEquals(11, sizeof($result));
    }
}


