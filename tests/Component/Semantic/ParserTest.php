<?php
namespace Tests\Stem\Semantic;

use App\Component\Semantic\Parser;
use Tests\TestCase;

class ParserTest extends TestCase
{
    /**
     * @dataProvider additionProvider
     */
    public function testSimple($text, $count, $erros)
    {
        $parser = new Parser($text);
        $listWords = $parser->get();

        $this->assertEquals($count, sizeof($listWords));
        $this->assertEquals($erros, $parser->hasError());
    }

    public function additionProvider()
    {
        return [
            ["Право заключения договора на поставку запасных частей для автомобилей марки ГАЗ для нужд ПАО «МРСК Центра и Приволжья» филиала «Нижновэнерго» (51305)", 19, true],
            ["Право на заключение договора на поставку запасных частей для автотранспорта марок МАЗ ЗИЛ КАМАЗ ГАЗ УАЗ ВАЗ", 14, false],
            ["Поставка запчастей для автомобильного транспорта (ГАЗ, ЗИЛ, УАЗ)", 7, false],
            ["Поставка запасных частей для автомобилей модельного ряда ГАЗ на 2017г", 8, true],
            ["Поставка запчастей к легковым, грузовым автомобилям, специализированной технике, автобусам и автомобилям марки ГАЗ", 10, false],
            ["на поставку запасных частей к транспортнымсредствам марки «Скания» для выполнения работ по техническому обслуживанию иремонту", 15, true]
        ];
    }
}