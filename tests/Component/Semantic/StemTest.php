<?php
namespace Tests\Stem\Semantic;

use App\Component\Semantic\Stem;
use Tests\TestCase;

class StemTest extends TestCase
{
    /**
     * @var Stem
     */
    private $service;

    public function setUp()
    {
        $this->service = new Stem();
    }

    public function testSimple()
    {
        $text = "Благородная львица заботиться о маленьких львятах";
        $result = $this->service->getListWord($text);

        $this->assertNotEmpty($result);
        $this->assertEquals(6, sizeof($result));
        $this->assertEquals(last($result), "львенок");
    }

    /**
     * @dataProvider additionProvider
     * @depends testSimple
     */
    public function testList($text, $count, $find, $actual)
    {
        $result = $this->service->getListWord($text);

        $this->assertEquals($count, sizeof($result));
        $this->assertNotEmpty($result[$find]);
        $this->assertEquals($result[$find], $actual);
    }

    public function additionProvider()
    {
        return [
            ["Право заключения договора на поставку запасных частей для автомобилей марки ГАЗ для нужд ПАО «МРСК Центра и Приволжья» филиала «Нижновэнерго» (51305)", 19, 10, "газ"],
            ["Право на заключение договора на поставку запасных частей для автотранспорта марок МАЗ ЗИЛ КАМАЗ ГАЗ УАЗ ВАЗ", 17, 15, "уаз"],
            ["Поставка запчастей для автомобильного транспорта (ГАЗ, ЗИЛ, УАЗ)", 8, 1, "запчасть"],
            ["Поставка запасных частей для автомобилей модельного ряда ГАЗ на 2017г", 9, 5, "модельный"],
            ["Поставка запчастей к легковым, грузовым автомобилям, специализированной технике, автобусам и автомобилям марки ГАЗ", 13, 4, "грузовой"],
            ["на поставку запасных частей к транспортнымсредствам марки «Скания» для выполнения работ по техническому обслуживанию иремонту", 15, 3, "часть"],
        ];
    }
}


