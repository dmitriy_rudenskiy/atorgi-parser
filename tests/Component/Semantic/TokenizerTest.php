<?php
namespace Tests\Stem\Semantic;

use App\Component\Semantic\Tokenizer;
use Tests\TestCase;

class TokenizerTest extends TestCase
{
    /**
     * @var Tokenizer
     */
    private $service;

    public function setUp()
    {
        $this->service = new Tokenizer();
    }

    /**
     * @param string $hash
     * @param string $text
     * @dataProvider additionProvider
     */
    public function testSimple($hash, $text)
    {
        $words = $this->service->tokenize($text);

        var_dump($words);
    }

    public function additionProvider()
    {
        return [
            ["", "Поставку запасных частей для погрузчиков L-34, Toyota, XCMG"],
            ["", "Погрузчик фронтальный L-34 Stalowa Wola"],
        ];
    }
}