<?php
namespace App\Component\Tools;

class RegexpTokenizer
{
    const MASK = "/[А-Я\-\']{2,}(?![а-я])|[А-Я\-\'][а-я\-\']+(?=[А-Я])|[\'\w\-]+/s";

    public function tokenize($text)
    {
        $text = str_replace('-', '', $text);

        $count = preg_match_all(
            self::MASK,
            mb_strtoupper($text, 'UTF-8'),
            $matches
        );

        if ($count < 1) {
            return null;
        }

        return $matches[0];
    }
}