<?php
namespace App\Component\Tools;

class Text
{
    const MASK_TEXT = '~([^A-ZА-Я0-9 ]+)~';

    const DELIMITER = ' ';

    const CHARSET = 'UTF-8';

    public function clearText($text)
    {
        // Перевод в верхний регистр
        $text = mb_strtoupper($text, self::CHARSET);

        // Замена ё на е
        $text = str_replace('Ё', 'Е', $text);

        // Точка сокращение слов, дефиз
        $text = str_replace(["\n", "\t", ".", ",", "-"], self::DELIMITER, $text);

        // удаляем двойные пробелы
        $text =  preg_replace('/ {2,}/', self::DELIMITER, $text);

        // удаляем не нужные символы
        $text =  preg_replace(self::MASK_TEXT, '', $text);

        return $text;
    }
}