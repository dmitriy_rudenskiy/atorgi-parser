<?php
namespace App\Component\Ftp;

use Elastica\Exception\RuntimeException;
use ModelBundle\Component\StateList;
use ModelBundle\Entity\CategoryRepository;
use ModelBundle\Entity\CodeCategoryRepository;
use ModelBundle\Entity\Document;
use ModelBundle\Entity\DocumentRepository;
use ModelBundle\Entity\DocumentWithFile;
use ModelBundle\Entity\FtpFileQueueRepository;
use ModelBundle\Entity\RegionRepository;
use ModelBundle\Entity\Tender;
use ModelBundle\Entity\TenderRepository;
use ModelBundle\Entity\Type;
use ModelBundle\Entity\TypeRepository;
use ModelBundle\Entity\User;
use ParserBundle\Service\GeneratorTrait;
use Touki\FTP\Model\File;
use ZipArchive;

class Main
{
    use GeneratorTrait;

    const NOTICE = 'Notice';



    private $dir;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Type
     */
    private $type;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var FtpFileQueueRepository
     */
    private $queue;

    /**
     * @var RegionRepository
     */
    private $regionRepository;

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var TypeRepository
     */
    private $typeRepository;

    /**
     * @var DocumentRepository
     */
    private $documentRepository;

    /**
     * @var CodeCategoryRepository
     */
    private $code;

    /**
     * @var TenderRepository
     */
    private $repositoryTender;

    public function __construct()
    {
        $this->dir = realpath(__DIR__ . '/../../../../../ftp');

        if (empty($this->dir)) {
            throw new \RuntimeException('Not find directory');
        }
    }

    /**
     * @return RegionRepository
     */
    public function getRegionRepository()
    {
        return $this->regionRepository;
    }

    /**
     * @param Region $regionRepository
     */
    public function setRegionRepository($regionRepository)
    {
        $this->regionRepository = $regionRepository;
    }

    /**
     * @return CategoryRepository
     */
    public function getCategoryRepository()
    {
        return $this->categoryRepository;
    }

    /**
     * @param CategoryRepository $categoryRepository
     */
    public function setCategoryRepository($categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @return TypeRepository
     */
    public function getTypeRepository()
    {
        return $this->typeRepository;
    }

    /**
     * @param TypeRepository $typeRepository
     */
    public function setTypeRepository($typeRepository)
    {
        $this->typeRepository = $typeRepository;
    }

    /**
     * @return DocumentRepository
     */
    public function getDocumentRepository()
    {
        return $this->documentRepository;
    }

    /**
     * @param DocumentRepository $documentRepository
     */
    public function setDocumentRepository($documentRepository)
    {
        $this->documentRepository = $documentRepository;
    }

    /**
     * @return FtpFileQueueRepository
     */
    public function getQueue()
    {
        return $this->queue;
    }

    /**
     * @param FtpFileQueueRepository $queue
     */
    public function setQueue($queue)
    {
        $this->queue = $queue;
    }

    /**
     * @return string
     */
    public function getDir()
    {
        return $this->dir;
    }

    /**
     * @param string $dir
     */
    public function setDir($dir)
    {
        $this->dir = $dir;
    }

    /**
     * @return CodeCategoryRepository
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param CodeCategoryRepository $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return TenderRepository
     */
    public function getRepositoryTender()
    {
        return $this->repositoryTender;
    }

    /**
     * @param TenderRepository $repositoryTender
     */
    public function setRepositoryTender($repositoryTender)
    {
        $this->repositoryTender = $repositoryTender;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param Type $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient(Client $client)
    {
        $this->client = $client;
    }


    public function run()
    {
        // составляем список файлов для загрузки (формируем очередь)
        $this->createListForDownload();

        // загружаем файлы
        do {
            $status = $this->download();
        } while ($status);

        // очищаем директории рекурсивно
        $this->clearFolder();

        // разбираем файлы и добавляем в базу
        $this->parse();
    }

    /**
     * Загрузка списка файлов для загрузки
     */
    public function createListForDownload()
    {
        $region = new Region();

        foreach ($region->getList() as $value) {

            $now = new \DateTime();
            $now->sub(new \DateInterval('P3D'));

            // ищем регион
            $regionEntity = $this->getRegionRepository()->find($value->id);

            $dir = sprintf(self::MASK_DOWNLOAD_FOLDER, $value->name);

            $listFiles = $this->client->ls($dir, true);

            foreach ($listFiles as $file) {
                /* @var File $file */

                // добавлены не позднее чем три дня назад и не берём за сегоднешнее число

                if ($file->getMtime() > $now) {
                    $this->getQueue()->add($this->getType(), $regionEntity, $file->getRealpath());
                }
            }
        }
    }

    /**
     * Загрузка файла и отметка о готовности
     *
     * @return bool
     */
    public function download()
    {
        $fileQueue = $this->getQueue()->next($this->getType());

        if ($fileQueue === null) {
            return false;
        }

        $dir = $this->getFolderForFiles(
            $fileQueue->getType()->getId(),
            $fileQueue->getRegion()->getId()
        );

        $tmp = explode(DIRECTORY_SEPARATOR, $fileQueue->getPath());
        $fileName =  $dir . DIRECTORY_SEPARATOR. end($tmp);

        // скачиваем файл
        try {
            $this->getClient()->download($fileQueue->getPath(), $fileName);
        } catch (\Exception $e) {
            // не удалось скачать
            echo "Main\t" . $e->getMessage() . "\n";
            $fileQueue->setState(1);
            $this->getQueue()->save();
        }


        if (file_exists($fileName)) {
            // распаковываем файл
            $this->unPack($fileName, $dir);
            unlink($fileName);

            // ставим отметку о загрузке файла
            $fileQueue->setState(1);
            $this->getQueue()->save();
        }

        return true;
    }

    /**
     * Распаковываем загруженные данные
     */
    public function unPack($fileName, $to)
    {
        $zip = new ZipArchive();

        if ($zip->open($fileName) === true) {
            $zip->extractTo($to);
            $zip->close();
        }
    }

    /**
     * Очистка директорий от лишних файлов
     */
    public function clearFolder()
    {
        foreach ($this->getListFileFromLocal() as $value) {
            if (strpos($value->getBasename(), '.xml') !== false
                && ($value->getSize() <= 198
                    || strpos(strtolower($value->getFilename()), 'otification') === false)) {
                unlink($value->getPathname());
            }
        }
    }

    /**
     * Разбираем файл
     */
    public function parse()
    {
        foreach ($this->getListFileFromLocal() as $value) {
            if (strpos($value->getBasename(), '.xml') !== false) {

                $params = $this->getParamsFile($value->getPathname());
                $content = file_get_contents($value->getPathname());
                $mapper = new Mapper44($content);

                if (!$mapper->isEmpty()) {
                    $this->mapping($mapper, $params->typeId, $params->regionId);
                }

                // удаляем файл
                unlink($value->getPathname());
            }
        }
    }

    /**
     * Разбираем данные из файла
     *
     * @param MapperInterface $mapper
     * @param $typeId
     * @param $regionId
     */
    public function mapping(MapperInterface $mapper, $typeId, $regionId)
    {
        $tender = new Tender();

        // тут проверяем наличие ОКПД
        $listCode = $mapper->getListCode();

        // v--- ваще ШЕДЕВР!!!

        if (strpos($mapper->getName(),'лизинг') !== false  ||
            strpos($mapper->getDescription(),'лизинг') !== false) {

            if (strpos($mapper->getName(),'транспорт') !== false  ||
                strpos($mapper->getDescription(),'транспорт') !== false ||
                strpos($mapper->getName(),'авто') !== false  ||
                strpos($mapper->getDescription(),'авто') !== false) {

                $category = $this->getCategoryRepository()->findOneBy(['slug' => 'c6']);
                $tender->addTag($category);
            }
        } elseif (sizeof($listCode) > 0) {
            // 001. поиск категорий
            foreach ($listCode as $value) {
                $code = $this->getCode()->findOneBy(['code' => $value]);

                // найдена категория и тендер не содержить её
                if ($code !== null) {
                    foreach ($code->getCategories() as $category) {
                        if (!$tender->getTags()->contains($category)) {
                            $tender->addTag($category);
                        }
                    }
                }
            }
        }

        // количество добавлееных категорий к тендеру
        if ($tender->getTags()->count() < 1) {
            return;
        }

        $type = $this->getTypeRepository()->find($typeId);

        // новые тендеры (Москва + область)
        if ($regionId == 42) {
            $regionId = 43;
        }

        $region = $this->getRegionRepository()->find($regionId);

        if ($region == null) {
            var_dump($regionId);
            return;
        }

        // 002. наполнение информацией
        $tender->setUser($this->user);
        $tender->setType($type);
        $tender->setRegion($region);
        $tender->setState(StateList::STATE_CHECKED);

        // Не нужно загружать файлы для этого тендера
        $tender->setIsReady(true);

        $tender->setNumber($this->getGenerator()->get());
        $tender->setUrl($mapper->getUrl());

        // длина заголовка
        if (mb_strlen($mapper->getName(), 'UTF-8') < 250) {
            $tender->setName( $mapper->getName());
        } else {
            $tender->setName(mb_strcut($mapper->getName(), 0, 250 ,'UTF-8') . ' ...');
        }

        $tender->setDescription($mapper->getDescription() . ', ' . implode(', ', $mapper->getAddDescription()));

        // проверка на длину текста
        if (mb_strlen($tender->getDescription(), 'UTF-8') > 255) {
            $tender->setDescription($mapper->getDescription());
        }

        $tender->setNameCompany($mapper->getNameCompany());
        $tender->setCode(substr(implode(',', $listCode), 0, 250));
        $tender->setPrice($mapper->getPrice());
        $tender->setStart($mapper->getStart());
        $tender->setFinish($mapper->getFinish());

        // 003. сохраняем результат
        if ($this->getRepositoryTender()->findOneBy(['url' => $tender->getUrl()]) !== null) {
            return;
        }

        $this->getRepositoryTender()->save($tender);

        // 004. сохраняем файлы
        $files = $mapper->getFiles();

        if (sizeof($files) < 1) {
            return;
        }

        foreach ($files as $value) {

            if ($this->getDocumentRepository()->findOneBy(['tender' => $tender, 'url' => $value->url]) === null) {
                $document = new DocumentWithFile();
                $document->setTender($tender);
                $document->setMime('empty');
                $document->setSize(1);
                $document->setName($value->name);
                $document->setUrl($value->url);

                $this->getDocumentRepository()->save($document);
            }
        }
    }

    /**
     * @param int $typeId
     * @param int $regionId
     * @return string
     */
    protected function getFolderForFiles($typeId, $regionId)
    {
        $dir = $this->dir . DIRECTORY_SEPARATOR
            . $typeId . DIRECTORY_SEPARATOR
            . $regionId;

        if (realpath($dir) === false) {
            mkdir($dir, 0775, true);
        }

        return $dir;
    }

    /**
     * @return \RecursiveIteratorIterator
     */
    protected function getListFileFromLocal()
    {
        $dir = realpath($this->dir . DIRECTORY_SEPARATOR . $this->getType()->getId());

        if (empty($dir)) {
            throw new RuntimeException('Not exists directory');
        }

        return  new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($dir),
            \RecursiveIteratorIterator::SELF_FIRST
        );
    }

    protected function getParamsFile($fileName)
    {
        $clear = str_replace($this->dir, '', $fileName);
        list(,$typeId, $regionId) = explode(DIRECTORY_SEPARATOR, $clear);

        return (object)[
            'typeId' => (int)$typeId,
            'regionId' => (int)$regionId
        ];
    }
}