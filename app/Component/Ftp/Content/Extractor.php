<?php
namespace App\Component\Ftp\Content;

use App\Entities\Ftp\File;
use App\Component\Ftp\Tools\File as Tools;

class Extractor
{
    private $tools;

    public function __construct()
    {
        $this->tools = new Tools();
    }

    /**
     * @param File $file
     * @param $to
     * @return \RecursiveIteratorIterator
     */
    public function extract(File $file, $to)
    {
        $filename = $this->tools->getPath($file);

        $this->extractTo($filename, $to);

        return $this->getListFile($to);
    }

    /**
     * @param $filename
     * @param $to
     */
    public function extractTo($filename, $to)
    {
        $filename = realpath($filename);
        $to = realpath($to);

        if ($filename === false || $to === false) {
            throw new \InvalidArgumentException();
        }

        $zip = new \ZipArchive();

        if ($zip->open($filename) !== true) {
            throw new \RuntimeException();
        }

        $zip->extractTo($to);
        $zip->close();
    }

    /**
     * @param string $dir
     * @return array
     */
    protected function getListFile($dir)
    {
        $list = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($dir),
            \RecursiveIteratorIterator::SELF_FIRST
        );

        $result = [];

        foreach ($list as $value) {
            $filename = $value->getRealPath();
            if (strpos($filename, '.xml') !== false) {
                $result[] = $filename;
            }
        }

        return $result;
    }
}