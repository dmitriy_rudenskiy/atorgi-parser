<?php
namespace App\Component\Ftp\Tools;

use App\Entities\Ftp\File as Entity;

class File
{
    /**
     * @var string
     */
    private $storageFiles;

    /**
     * @var string
     */
    private $storageTmp;


    const FILE_EXTENSION = '.zip';

    public function __construct()
    {
        $this->storageFiles = '/Volumes/disk/ftp';
        // $this->storageFiles = storage_path('ftp/zip');

    }


    public function getPath(Entity $file)
    {
        $dir = $this->storageFiles
            . DIRECTORY_SEPARATOR
            . substr($file->hash, 0, 2);

        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        $filename = $dir . DIRECTORY_SEPARATOR . $file->hash . self::FILE_EXTENSION;

        return $filename;
    }
}