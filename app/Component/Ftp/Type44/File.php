<?php
namespace App\Component\Ftp\Type44;

use App\Component\Ftp\Download\FileBase;

class File extends FileBase
{
    protected function getType()
    {
        return '44';
    }

    protected function initClient()
    {
        return new Client();
    }
}