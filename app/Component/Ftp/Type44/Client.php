<?php
namespace App\Component\Ftp\Type44;

use App\Component\Ftp\Download\ClientBase;

class Client extends ClientBase
{
    protected function getHost()
    {
        return 'ftp.zakupki.gov.ru';
    }

    protected function getLogin()
    {
        return 'free';
    }

    protected function getPassword()
    {
        return 'free';
    }

    public function getListFolder()
    {
        return [
            './fcs_regions/%s/notifications/currMonth/'
        ];
    }
}