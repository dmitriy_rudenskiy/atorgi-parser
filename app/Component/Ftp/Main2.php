<?php
namespace App\Component\Ftp;

class Main2 extends Main
{
    public function run()
    {
        // составляем список файлов для загрузки (формируем очередь)
        $this->createListForDownload();

        // загружаем файлы
        do {
            $status = $this->download();
        } while ($status);

        // очищаем директории рекурсивно
        $this->clearFolder();

        // разбираем файлы и добавляем в базу
        $this->parse();
    }

    /**
     * Разбираем файл
     */
    public function parse()
    {

        foreach ($this->getListFileFromLocal() as $value) {
            if (strpos($value->getBasename(), '.xml') !== false) {

                $params = $this->getParamsFile($value->getPathname());
                $content = file_get_contents($value->getPathname());
                $mapper = new Mapper223($content);

                if (!$mapper->isEmpty() && $mapper->getListCode() !== null) {
                    $this->mapping($mapper, $params->typeId, $params->regionId);
                }

                // удаляем файл
                unlink($value->getPathname());
            }
        }
    }

    /**
     * Загрузка списка файлов для загрузки
     */
    public function createListForDownload()
    {
        $now = new \DateTime();
        $now->sub(new \DateInterval('P30D'));

        foreach ($this->getRegion() as $key => $value) {

            // ищем регион
            $regionEntity = $this->getRegionRepository()->find($value);

            foreach ($this->getListMask() as $item) {
                $dir = sprintf($item, $key);

                echo sprintf("Load files from %s\n", $dir);

                try {
                    $listFiles = $this->getClient()->ls($dir, true);
                } catch (\Exception $e) {

                    // Directory not found
                    echo "Main2\t" . $e->getMessage() . "\n";
                }

                echo sprintf("Find files %s\n", sizeof($listFiles));

                if (!empty($listFiles)) {
                    foreach ($listFiles as $file) {

                        // добавлены не позднее чем три дня назад и маленького размера
                        if ($file->getMtime() > $now && $file->getSize() > 258) {
                            $this->getQueue()->add($this->getType(), $regionEntity, $file->getRealpath());
                        }
                    }
                }
            }
        }
    }

    public function clearFolder()
    {
        foreach ($this->getListFileFromLocal() as $value) {
            if (strpos($value->getBasename(), '.xml') !== false && $value->getSize() <= 198 ) {
                unlink($value->getPathname());
            }
        }
    }

    protected function getListMask()
    {
        return [

        ];
    }

    protected function getRegion()
    {
        return ;
    }

}