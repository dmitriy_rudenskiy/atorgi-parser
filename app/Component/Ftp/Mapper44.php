<?php
namespace App\Component\Ftp;


class Mapper44 implements MapperInterface
{
    private $root;
    
    public function __construct($content)
    {
        $content = preg_replace('~<signature (.*?)</signature>~', '', $content);

        if (strpos($content, 'OKPD2') === false) {
            return;
        }

        $text = str_replace('ns2:', '', $content);

        $xml = simplexml_load_string($text);

        $this->root = $xml->children()[0];
    }

    public function getListCode()
    {
        $list = [];

        if (!empty($this->root->lots)) {
            foreach ($this->root->lots as $lot) {
                foreach ($lot->lot->purchaseObjects->purchaseObject as $value) {
                    $list[] =  $value->OKPD2->code->__toString();
                }
            }

            return array_unique($list);
        }

        if (!empty($this->root->lot)) {
            foreach ($this->root->lot->purchaseObjects->purchaseObject as $value) {
                $list[] = $value->OKPD2->code->__toString();
            }

            return array_unique($list);
        }
    }

    public function getAddDescription()
    {
        $list = [];

        if (!empty($this->root->lots)) {
            foreach ($this->root->lots as $lot) {
                foreach ($lot->lot->purchaseObjects->purchaseObject as $value) {
                    $list[] = $this->clearText($value->name->__toString());


                }
            }

            return array_unique($list);
        }

        if (!empty($this->root->lot)) {
            foreach ($this->root->lot->purchaseObjects->purchaseObject as $value) {
                $list[] = $this->clearText($value->name->__toString());
            }

            return array_unique($list);
        }
    }
    
    public function getUrl()
    {
        return $this->root->href->__toString();
    }

    public function getName()
    {
        $name = $this->root->purchaseObjectInfo->__toString();

        return trim(str_replace(["\n", "\t"], ['', ''], $name));
    }

    public function getDescription()
    {
        return $this->root->placingWay
            ->name
            ->__toString();

    }

    public function getNameCompany()
    {
        return $this->root->purchaseResponsible
            ->responsibleOrg
            ->fullName
            ->__toString();
    }

    public function getStart()
    {
        if (!empty($this->root->procedureInfo->collecting->startDate)) {

            $date = $this->root->procedureInfo
                ->collecting
                ->startDate
                ->__toString();

            return new \DateTime($date);
        }

        return new \DateTime();
    }

    public function getFinish()
    {
        if (!empty($this->root->procedureInfo->collecting->endDate)) {

            $date = $this->root->procedureInfo
                ->collecting
                ->endDate
                ->__toString();

            return new \DateTime($date);
        }

        $date = $this->getStart();
        $date->add(new \DateInterval('P15D'));

        return $date;
    }

    public function getPrice()
    {
        $price = 0;

        if (!empty($this->root->lot)) {
            $price = $this->root->lot
                ->purchaseObjects
                ->totalSum
                ->__toString();
        }

        if (!empty($this->root->lots)) {
            $price = $this->root->lots->lot
                ->purchaseObjects
                ->totalSum
                ->__toString();
        }


        return (int)round(floatval($price));
    }

    public function isEmpty()
    {
        return $this->root === null;
    }

    public function getFiles()
    {
        $files = [];

        foreach ($this->root->attachments as $value) {

            $file = (object)[
                'name' => $value->attachment->fileName ->__toString(),
                'url' => $value->attachment->url->__toString()
            ];

            $files[] = $file;
        }

        return $files;
    }

    protected function clearText($text)
    {
        $text = str_replace('Согласно требованиям документации', '', $text);
        $text = str_replace('ё', 'е', $text);

        $text = mb_strtoupper($text, 'UTF-8');
        $text = preg_replace('/ {2,}/', ' ', $text);
        $text = preg_replace('~([^A-ZА-Я0-9 ]+)~', '', $text);
        $text = mb_strtolower($text, 'UTF-8');

        return trim($text);
    }
}