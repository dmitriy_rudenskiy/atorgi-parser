<?php
namespace App\Component\Ftp;

interface MapperInterface
{
    /**
     * @return array|null
     */
    public function getListCode();

    /**
     * @return string
     */
    public function getUrl();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @return string
     */
    public function getNameCompany();

    /**
     *
     * @return \DateTime
     */
    public function getFinish();

    public function getStart();

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice();


    public function isEmpty();

    public function getFiles();

}