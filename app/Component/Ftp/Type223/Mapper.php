<?php
namespace App\Component\Ftp\Type223;

use App\Component\Ftp\Download\MapperBase;

class Mapper extends MapperBase
{
    public function read($filename)
    {
        $content = file_get_contents($filename);

        if (empty($content)) {
            return self::EMPTY_FILE;
        }

        $content = preg_replace('~<signature (.*?)</signature>~', '', $content);
        $text = str_replace('ns2:', '', $content);

        $xml = simplexml_load_string($text);
        $data = json_decode(json_encode($xml), true);

        if (!is_array($data)) {
            return self::EMPTY_CONTENT;
        }

        if (isset($data["body"]["item"])) {
            foreach ($data["body"]["item"] as $key => $value) {
                if (strpos($key, 'purchaseNotice') !== false) {
                    $this->content = $value;
                    return self::READ_FILE;
                }
            }
        }

        var_dump('00', array_keys($data["body"]["item"]));
        exit();
    }

    public function getRegistrationNumber()
    {
        return $this->content["registrationNumber"];
    }

    public function getListCode()
    {
        // TODO: Implement getListCode() method.
    }

    public function getUrl()
    {
        // TODO: Implement getUrl() method.
    }

    public function getName()
    {
        return $this->clearText($this->content["name"]);
    }

    public function getDescription()
    {
        $list = [];

        foreach($this->content["lots"] as $value) {
            if (!empty($value["lotData"]["subject"])) {
                $list[] = $value["lotData"]["subject"];
            }
        }

        return $this->clearText(implode(', ', $list));
    }

    public function getNameCompany()
    {
        if (!isset($this->content["customer"]["mainInfo"]["shortName"])) {
            return null;
        }

        return $this->clearText($this->content["customer"]["mainInfo"]["shortName"]);
    }

    public function getStart()
    {
        // TODO: Implement getStart() method.
    }

    public function getFinish()
    {
        // TODO: Implement getFinish() method.
    }

    public function getPrice()
    {
        // TODO: Implement getPrice() method.
    }

    public function getFiles()
    {
        if (!isset($this->content["attachments"]["totalDocumentsCount"])
            || $this->content["attachments"]["totalDocumentsCount"] < 1
        ) {
            return null;
        }

        $result = [];

        foreach ($this->content["attachments"]["document"] as $value) {
            $result[] = [
                'guid' => $value["guid"],
                'create' => strtotime(str_replace('T', '', $value["createDateTime"])),
                'name' => $value["fileName"],
                'url' => $value["url"]
            ];
        }

        return $result;
    }
}