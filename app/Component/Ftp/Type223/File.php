<?php
namespace App\Component\Ftp\Type223;

use App\Component\Ftp\Download\FileBase;

class File extends FileBase
{
    protected function getType()
    {
        return '223';
    }

    protected function initClient()
    {
        return new Client();
    }
}