<?php
namespace App\Component\Ftp\Type223;

use App\Component\Ftp\Download\ClientBase;

class Client extends ClientBase
{
    protected function getHost()
    {
        return 'ftp.zakupki.gov.ru';
    }

    protected function getLogin()
    {
        return 'fz223free';
    }

    protected function getPassword()
    {
        return 'fz223free';
    }

    public function getListFolder()
    {
        return [
            "/out/published/%s/purchaseNotice/daily/",
            "/out/published/%s/purchaseNoticeAE/daily/",
            "/out/published/%s/purchaseNoticeAE94/daily/",
            "/out/published/%s/purchaseNoticeEP/daily/",
            "/out/published/%s/purchaseNoticeIS/daily/",
            "/out/published/%s/purchaseNoticeOA/daily/",
            "/out/published/%s/purchaseNoticeOK/daily/",
            "/out/published/%s/purchaseNoticeZK/daily/"
        ];
    }
}