<?php
namespace App\Component\Ftp\Download;

use App\Entities\Catalog\Region;
use App\Entities\Ftp\File;

class Diff
{
    /**
     * @param ClientBase $ftp
     * @param Region[] $cities
     */
    public function getListFiles(ClientBase $ftp, $cities)
    {
        foreach ($cities as $city) {
            foreach ($ftp->getListFolder() as $key => $mask) {
                $path = sprintf($mask, $city->name);
                $list = $ftp->getList($path);
                $this->addToQueue($key, $city->id, $list);
            }
        }
    }

    protected function addToQueue($pathId, $regionId, $listUrl)
    {
        foreach ($listUrl as $value) {
            File::add($pathId, $regionId, $value);
        }
    }
}