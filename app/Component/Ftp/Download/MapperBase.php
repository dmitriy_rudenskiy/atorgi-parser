<?php
namespace App\Component\Ftp\Download;

abstract class MapperBase
{
    protected $content;

    const EMPTY_FILE = 1;

    const EMPTY_CONTENT = 2;

    const READ_FILE = 5;

    /**
     * @param string $filename
     * @return int
     */
    abstract public function read($filename);

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return ($this->content === null);
    }

    /**
     * @return array
     */
    public function get()
    {
        return $this->content;
    }

    /**
     * @return mixed
     */
    abstract public function getListCode();

    /**
     * @return mixed
     */
    abstract public function getUrl();

    /**
     * @return mixed
     */
    abstract public function getName();

    /**
     * @return mixed
     */
    abstract public function getDescription();

    /**
     * @return mixed
     */
    abstract public function getNameCompany();

    /**
     * @return mixed
     */
    abstract public function getStart();

    /**
     * @return mixed
     */
    abstract public function getFinish();

    /**
     * @return mixed
     */
    abstract public function getPrice();

    /**
     * @return mixed
     */
    abstract public function getFiles();

    /**
     * @param string $text
     * @return string
     */
    protected function clearText($text)
    {
        $text = str_replace("\n", ' ', $text);
        $text = str_replace("\t", ' ', $text);
        $text = str_replace('"', ' ', $text);
        $text = str_replace('  ', ' ', $text);

        return trim($text);
    }
}