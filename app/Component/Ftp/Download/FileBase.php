<?php
namespace App\Component\Ftp\Download;

use App\Entities\Ftp\File;

abstract class FileBase
{
    /**
     * @var ClientBase
     */
    private $client;

    public function run()
    {
        $queryBuilder = File::getQueryFrom($this->getType())
            ->where('downloaded', 0);

        $count = $queryBuilder->count();

        if ($count < 1) {
            return false;
        }

        do {
            $file = $queryBuilder->first();

            if ($file !== null) {
                $this->load($file);
            }
        } while ($file !== null);
    }

    protected function load(File $file)
    {
        $filename = (new \App\Component\Ftp\Tools\File())->getPath($file);

        $status = $this->getClient()->download($filename, $file->url);

        if ($status) {
            $file->update(['downloaded' => true]);
        }
    }

    /**
     * @return ClientBase
     */
    protected function getClient()
    {
        if ($this->client === null) {
            $this->client = $this->initClient();
        }

        return $this->client;
    }

    /**
     * @return ClientBase
     */
    abstract protected function initClient();
}