<?php
namespace App\Component\Ftp\Download;

abstract class ClientBase
{
    private $connectId;

    public function __construct()
    {
        $this->connectId = ftp_connect($this->getHost());
        $status = ftp_login($this->connectId, $this->getLogin(), $this->getPassword());

        if ($status !== true) {
            throw new \RuntimeException();
        }

        ftp_pasv($this->connectId, true);
    }

    public function __destruct()
    {
        ftp_close($this->connectId);
    }

    public function getRoot()
    {
        return ftp_nlist($this->connectId, DIRECTORY_SEPARATOR);
    }

    public function getList($path)
    {
        return ftp_nlist($this->connectId, $path);
    }

    protected function getConnect()
    {
        return $this->connectId;
    }

    public function download($src, $dst)
    {
        ftp_get($this->connectId, $src, $dst, FTP_BINARY);

        return file_exists($src);
    }

    /**
     * @return string
     */
    abstract protected function getHost();

    /**
     * @return string
     */
    abstract protected function getLogin();

    /**
     * @return string
     */
    abstract protected function getPassword();

    /**
     * @return string[]
     */
    abstract public function getListFolder();
}