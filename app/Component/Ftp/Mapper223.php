<?php
namespace App\Component\Ftp;


class Mapper223 implements MapperInterface
{
    private $root;

    public function __construct($content)
    {
        // без кода ОКПД2 тендер тяжело распознать
        if (strpos($content, 'okpd2') === false) {
            return;
        }

        // очищаем контент
        $content = str_replace('ns2:', '', $content);

        // берём нужную часть
        preg_match('~<body>.+</body>~s', $content, $matches);

        if (empty($matches[0])) {
            return;
        }

        $xml = simplexml_load_string($matches[0]);

        $this->root = $xml->children()[0]->purchaseNoticeData;
    }

    public function getListCode()
    {
        $list = [];

        if (!empty($this->root->lots)) {

            foreach ($this->root->lots->lot as $value) {
                foreach ($value->lotData->lotItems as $item) {
                    $list[] = $item->lotItem->okpd2->code->__toString();
                }
            }
            return array_unique($list);
        }
    }

    public function getUrl()
    {
        return 'http://zakupki.gov.ru/epz/order/quicksearch/search_eis.html?searchString=' .
            trim($this->root->registrationNumber->__toString())
            . '&morphology=off';
    }

    public function getName()
    {
        return trim($this->root->name->__toString());
    }

    public function getDescription()
    {
        return trim($this->root->purchaseCodeName->__toString());
    }

    public function getAddDescription()
    {
        $list = [];

        if (!empty($this->root->lots)) {

            foreach ($this->root->lots->lot as $value) {
                foreach ($value->lotData->lotItems as $item) {
                    $tmp = $this->clearText($item->lotItem->additionalInfo);

                    if (!empty($tmp)) {
                        $list[] = $tmp;
                    }
                }
            }
        }

        return $list;
    }

    public function getNameCompany()
    {
        return $this->root->customer->mainInfo->shortName->__toString();
    }

    public function getFinish()
    {
        if (!empty($this->root->documentationDelivery->deliveryEndDateTime)) {

            $date = $this->root
                ->documentationDelivery
                ->deliveryEndDateTime
                ->__toString();
            
            return new \DateTime($date);
        }

        $date = $this->getStart();
        $date->add(new \DateInterval('P15D'));

        return $date;
    }

    public function getStart()
    {
        if (!empty($this->root->documentationDelivery->deliveryStartDateTime)) {

            $date = $this->root
                ->documentationDelivery
                ->deliveryStartDateTime
                ->__toString();

            return new \DateTime($date);
        }

        return new \DateTime();
    }

    public function getPrice()
    {
        $price = 0;

        if (!empty($this->root->lots)) {
            $price = $this->root
                ->lots
                ->lot
                ->lotData
                ->initialSum
                ->__toString();
        } else {
            echo "Error price";
            exit();
        }


        return (int)round(floatval($price));
    }

    public function isEmpty()
    {
        return $this->root === null;
    }

    public function getFiles()
    {
        $files = [];

        foreach ($this->root->attachments->document as $value) {

            $files[] = (object)[
                'name' => $value->fileName ->__toString(),
                'url' => $value->url->__toString()
            ];
        }

        return $files;
    }

    protected function clearText($text)
    {
        $text = str_replace('Согласно требованиям документации', '', $text);
        $text = str_replace('ё', 'е', $text);

        $text = mb_strtoupper($text, 'UTF-8');
        $text = preg_replace('/ {2,}/', ' ', $text);
        $text = preg_replace('~([^A-ZА-Я0-9 ]+)~', '', $text);
        $text = mb_strtolower($text, 'UTF-8');

        return trim($text);
    }
}