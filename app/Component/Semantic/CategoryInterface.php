<?php
namespace App\Component\Semantic;


interface CategoryInterface
{
    /**
     * Грузовой автомобиль
     */
    const TRUCK = 1;

    /**
     * Легковой автомобиль
     */
    const CAR = 2;

    /**
     * Закупка автотранспорта и спецтехники
     */
    const BUY = 3;

    /**
     * Техническое обслуживание и ремонт автомобиля
     */
    const REPAIR = 4;

    /**
     * Детали машин
     */
    const SPARE_PART = 5;

    /**
     * Иностранные автомобили
     */
    const FOREIGN = 6;

    /**
     * Отечественные автомобили
     */
    const HOME = 7;

    /**
     *
     */
    const TIRE = 8;

    const TRASH = 9;
}
