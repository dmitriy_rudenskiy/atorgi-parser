<?php

namespace App\Component\Semantic;

class Cluster implements CategoryInterface
{
    private $storage;

    public function __construct($data)
    {
        if (!is_array($data)) {
            return;
        }

        $this->storage = [
            self::TRUCK => 0,
            self::CAR => 0,
            self::BUY => 0,
            self::REPAIR => 0,
            self::SPARE_PART => 0,
            self::FOREIGN => 0,
            self::HOME => 0,
            self::TIRE => 0,
            self::TRASH => 0
        ];

        foreach ((array)$data as $key => $value) {
            $this->storage[$key] = $value;
        }
    }

    public function get()
    {
        // нет слов для анализа
        if ($this->isEmpty()) {
            return null;
        }

        // это точно не нужный тендер
        if ($this->storage[self::TRASH] > 0) {
            return null;
        }

        if ($this->isBuy()) {
            return null;
        }


        if ($this->isTire()) {
            //return 'Автошины';
            return null;
        }

        // не запчасть
        if (!$this->isPart()) {
            return null;
        }

        // грузовик
        if ($this->isTruck()) {
            // импортная или отчественная
            if ($this->isImport()) {
                return 'Автозапчасть на импортный грузовик';
            } elseif ($this->isHome()) {
                return 'Автозапчасть на отечественый грузовик';
            } else {
                return 'Запасная часть для грузовика';
            }
        }

        // легковая машина
        if ($this->isCar()) {
            return 'Автозапчасть на легковой';
        }

        return null;
    }

    protected function isTire()
    {
        return $this->storage[self::TIRE] > 5;
    }

    protected function isBuy()
    {
        return ($this->storage[self::BUY] > 5
            && $this->storage[self::BUY] > $this->storage[self::REPAIR]
            && ($this->isCar() || $this->isTruck()));
    }

    protected function isService()
    {
        return ($this->storage[self::REPAIR] > 0 && $this->storage[self::CAR] > 5);
    }

    public function isPart()
    {
        if ($this->storage[self::SPARE_PART] < 1) {
            return false;
        }

        if ($this->storage[self::SPARE_PART] >= $this->storage[self::BUY]
            && $this->storage[self::SPARE_PART] >= $this->storage[self::REPAIR]) {
            return true;
        }

        return false;
    }

    public function isTruck()
    {
        // существуют призноки крупногабаритной техники
        if ($this->storage[self::TRUCK] < 1) {
            return false;
        }

        if ($this->storage[self::TRUCK] >= $this->storage[self::CAR]) {
            return true;
        }

        return false;
    }

    protected function isCar()
    {
        if ($this->storage[self::CAR] < 5) {
            return false;
        }

        if ($this->storage[self::CAR] > $this->storage[self::TRUCK]) {
            return true;
        }

        return false;
    }


    public function isImport()
    {
        if ($this->storage[self::FOREIGN] < 1) {
            return false;
        }

        if ($this->storage[self::FOREIGN] > $this->storage[self::HOME]) {
            return true;
        }

        return false;
    }

    public function isHome()
    {
        if ($this->storage[self::HOME] < 1) {
            return false;
        }

        if ($this->storage[self::HOME] > $this->storage[self::FOREIGN]) {
            return true;
        }

        return false;
    }

    public function isEmpty()
    {
        if ($this->storage === null) {
            return true;
        }

        foreach ($this->storage as $value) {
            if (!empty($value)) {
                return false;
            }
        }

        return true;
    }
}


