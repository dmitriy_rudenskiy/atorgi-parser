<?php
namespace App\Component\Semantic;


class Select
{
    /**
     * @param array $data
     * @return array
     */
    public function find(array $data)
    {
        if (sizeof($data) < 1) {
            return null;
        }

        $sql = "SELECT w.lemma, r.grade, c.description
                FROM catalog_word w
                LEFT JOIN catalog_word_to_category r ON r.word_id=w.id
                LEFT JOIN catalog_category c ON c.id=r.category_id
                WHERE w.lemma IN('" . implode("', '", $data). "')
                ORDER BY  w.lemma
                LIMIT 100;";

        return app('db')->select($sql);
    }

    /**
     * @param array $data
     * @return array
     */
    public function getGroup(array $data)
    {
        if (sizeof($data) < 1) {
            return null;
        }

        $sql = "SELECT c.id, SUM(r.grade) grade
                FROM catalog_word w
                  INNER JOIN catalog_word_to_category r ON r.word_id=w.id
                  INNER JOIN catalog_category c ON c.id=r.category_id
                WHERE w.lemma IN('" . implode("', '", $data). "')
                GROUP BY c.id
                ORDER BY SUM(r.grade) DESC";

        $list = app('db')->select($sql);

        $result = [];

        foreach ($list as $value) {
            $result[$value->id] = (int)$value->grade;
        }

        return $result;
    }
}