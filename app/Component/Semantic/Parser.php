<?php
namespace App\Component\Semantic;

/**
 * Разбираем строку на слова
 *
 * Class Parser
 * @package App\Component\Semantic
 */
class Parser implements CharInterface
{
    /**
     * @var array
     */
    private $listWords = [];

    /**
     * @var int
     */
    private $errors = 0;

    public function __construct($text, $checkError = false)
    {
        if ($checkError === true) {
            // проверяем орфографические ошибки
            $speller = new Speller();
            $errors = $speller->check($text);

            // есть ошибки
            $this->errors = sizeof($errors);
            if ($this->errors > 0) {
                $text .= self::WORD_SEPARATOR . implode(self::WORD_SEPARATOR, $errors);
            }
        }

        $tokenizer = new Tokenizer();
        $tokens = $tokenizer->tokenize($text);

        // нет токенов
        if (sizeof($tokens) < 1) {
            return null;
        }

        $tokens = array_unique($tokens);
        sort($tokens);

        // удаляем стоп слова
        $stopWords = new StopWords();
        $tokens = $stopWords->clear($tokens);

        // морфология
        $morphology = new Morphology();

        foreach ($tokens as $key => $value) {

            $tokens[$key] = mb_strtolower($value, Tokenizer::CHARSET);

            $numeric = (bool)preg_match("/[0-9]{1,}/", $value);

            if (!$numeric) {
                $lemma = $morphology->normalized($value);

                if ($lemma !== null) {
                    $tokens[$key] = $lemma;
                }
            }
        }

        $this->listWords = array_unique($tokens);
        sort($this->listWords);

    }

    public function get()
    {
        return $this->listWords;
    }

    public function hasError()
    {
        return $this->errors > 0;
    }
}