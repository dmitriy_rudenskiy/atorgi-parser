<?php
namespace App\Component\Semantic;

use Illuminate\Support\Facades\Cache;
use Mystem\Mystem;

class Morphology
{
    /**
     * @param $word
     * @return mixed
     * @throws \Exception
     */
    public function normalized($word)
    {
        // TODO: добавить кэширование
        // $lemma = Cache::get('key');

        $stem = Mystem::stemm($word);

        if (empty($stem[0]['analysis'][0]['lex'])) {
            return null;
        }

        return $stem[0]['analysis'][0]['lex'];
    }
}