<?php
namespace App\Component\Semantic;

class Tokenizer
{
    const MASK_TEXT = '~([^A-ZА-Я0-9 ]+)~';

    const DELIMITER = ' ';

    const CHARSET = 'UTF-8';

    public function tokenize($text)
    {
        return explode(self::DELIMITER, $this->clear($text));
    }

    protected function clear($text)
    {
        // Перевод в верхний регистр
        $text = mb_strtoupper($text, self::CHARSET);

        $text = str_replace(['«', '»'], '', $text);

        // Замена ё на е
        $text = str_replace('Ё', 'Е', $text);

        // Точка сокращение слов, дефиз
        $tmp1 = str_replace(['.', ',', '-'], [' ', ' ', ' '], $text);
        $tmp2 = str_replace('-', '', $text);

        $text = $tmp1 . ' ' . $tmp2;

        // удаляем двойные пробелы
        $text =  preg_replace('/ {2,}/', ' ', $text);

        // удаляем не нужные символы
        $text =  preg_replace(self::MASK_TEXT, '', $text);

        return $text;
    }
}