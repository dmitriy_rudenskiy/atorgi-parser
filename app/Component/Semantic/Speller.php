<?php
namespace App\Component\Semantic;

use Mekras\Speller\Hunspell\Hunspell;
use Mekras\Speller\Source\StringSource;

class Speller
{
    /**
     * @var Hunspell
     */
    private $service;

    public function __construct()
    {
        $this->service = new Hunspell('/usr/local/bin/hunspell');
        $this->service->setDictionaryPath(storage_path('dict'));
    }

    public function check($text)
    {
        $source = new StringSource($text);
        $issues = $this->service->checkText($source, ['ru_RU', 'ru']);

        $result = [];

        foreach ($issues as $value) {
            if (isset($value->suggestions[0])) {
                $result[] = $value->suggestions[0];
            }
        }

        return $result;
    }
}