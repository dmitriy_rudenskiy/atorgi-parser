<?php

namespace App\Entities\Ftp;

use App\Entities\Catalog\Region;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table = 'ftp_file';

    protected $fillable = ['downloaded', 'unpacked', 'path_id', 'region_id', 'hash', 'url'];

    public static function has($hash)
    {
        $file = self::where('hash', $hash)->first();

        return ($file !== null);
    }

    /**
     * @param int $pathId
     * @param int $regionId
     * @param string $url
     */
    public static function add($pathId, $regionId, $url)
    {
        $hash = md5($url);

        if (self::has($hash)) {
            return;
        }

        $data = [
            'path_id' => $pathId,
            'region_id' => $regionId,
            'hash' => $hash,
            'url' => $url
        ];

        self::create($data);
    }

    public function region()
    {
        return $this->belongsTo(Region::class, 'region_id');
    }


    /**
     * @param $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getQueryFrom($type)
    {
        return self::select('ftp_file.*')
            ->with('region')
            ->join('catalog_region', 'catalog_region.id', '=', 'ftp_file.region_id')
            ->where('catalog_region.type', $type);
    }
}