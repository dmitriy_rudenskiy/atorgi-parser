<?php

namespace App\Entities\Catalog;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'catalog_region';

    protected $fillable = ['id', 'name'];

    public $timestamps = false;

}