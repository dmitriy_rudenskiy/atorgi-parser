<?php

namespace App\Entities\Catalog;

use Illuminate\Database\Eloquent\Model;

class WordToCategory extends Model
{
    protected $table = 'catalog_word_to_category';

    protected $fillable = ['word_id', 'category_id', 'grade'];

    public $timestamps = false;

    public function add($wordId, $categoryId)
    {
        $data = [
            'word_id' => $wordId,
            'category_id' => $categoryId
        ];

        $tmp = $this->where($data)->first();

        if ($tmp !== null) {
            return false;
        }

        $data['grade'] = 10;

        return $this->create($data);
    }
}