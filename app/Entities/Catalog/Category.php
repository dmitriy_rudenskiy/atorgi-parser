<?php

namespace App\Entities\Catalog;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'catalog_category';

    protected $fillable = ['alias', 'description'];

    public $timestamps = false;

}