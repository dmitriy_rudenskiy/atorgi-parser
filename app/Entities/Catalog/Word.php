<?php

namespace App\Entities\Catalog;

use Illuminate\Database\Eloquent\Model;

class Word extends Model
{
    protected $table = 'catalog_word';

    protected $fillable = ['is_active', 'lemma'];

    public $timestamps = false;

    public function get($lemma)
    {
        $word = $this->where('lemma', $lemma)->first();

        if ($word !== null) {
            return $word;
        }

        return $this->create(['lemma' => $lemma]);
    }

    public function category()
    {
        return $this->belongsToMany(Category::class, 'catalog_word_to_category', 'word_id', 'category_id');
    }
}
