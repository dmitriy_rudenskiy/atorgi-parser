<?php

namespace App\Console;

use App\Console\Commands\CreateLIstFromFtp;
use App\Console\Commands\FeatureExtraction\ConvertToRaw;
use App\Console\Commands\FeatureExtraction\ExportRawDataInFile;
use App\Console\Commands\GetCategory;
use App\Console\Commands\ImportWords;
use App\Console\Commands\LoadFromFtp;
use App\Console\Commands\ReadFiles;
use App\Console\Commands\Tools\FindLemma;
use App\Console\Commands\Tools\TestText;
use App\Console\Commands\Verifier;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ImportWords::class,
        Verifier::class,
        FindLemma::class,
        ReadFiles::class,
        GetCategory::class,
        LoadFromFtp::class,
        CreateLIstFromFtp::class,
        ConvertToRaw::class,
        ExportRawDataInFile::class,
        TestText::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //
    }
}
