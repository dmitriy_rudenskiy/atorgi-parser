<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Component\Ftp\Download\Diff;
use App\Component\Ftp\Type223\Client as Client223;
use App\Component\Ftp\Type44\Client as Client44;
use App\Entities\Catalog\Region;

class CreateLIstFromFtp extends Command
{
    protected $signature = 'ftp:list:files';

    protected $description = 'Create list files from ftp for download';

    public function handle()
    {
        $this->line('Start read for fz44');

        $cities = Region::where('type', '44')->get();
        $ftp = new Client44();
        $diff = new Diff();
        $diff->getListFiles($ftp, $cities);

        $this->line('Start read for fz223');

        $cities = Region::where('type', '223')->get();
        $ftp = new Client223();
        $diff = new Diff();
        $diff->getListFiles($ftp, $cities);

        $this->line('Finish work.');
    }
}








