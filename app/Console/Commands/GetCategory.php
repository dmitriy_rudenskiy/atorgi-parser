<?php
namespace App\Console\Commands;

use App\Component\Semantic\Cluster;
use App\Component\Semantic\Parser;
use App\Component\Semantic\Select;
use Illuminate\Console\Command;


/**
 * Class GetCategory
 * @package App\Console\Commands
 */
class GetCategory extends Command
{
    protected $signature = 'semantic:get:category {text}';

    protected $description = 'Semantic analysis for search';

    public function handle()
    {
        $text = $this->argument('text');

        $parser = new Parser($text, false);
        $select = new Select();
        $result = $select->getGroup($parser->get());

        $group = new Cluster($result);
        $category = $group->get();

        $this->line($category);
    }
}








