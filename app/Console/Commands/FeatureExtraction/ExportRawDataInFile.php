<?php
namespace App\Console\Commands\FeatureExtraction;

use App\Component\Ftp\Content\Extractor;
use App\Component\Ftp\Type223\Mapper;
use App\Component\Tools\RegexpTokenizer;
use App\Entities\Ftp\File;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ExportRawDataInFile extends Command
{
    protected $signature = 'semantic:export:raw';

    protected $description = 'Data collection';

    protected $filename = '/Volumes/disk/data.csv';

    const START_ID = 2103912;

    const ALL_RECORDS = 3159851;

    const STEP = 50;

    public function handle()
    {
        if (file_exists($this->filename) && self::START_ID === null) {
            unlink($this->filename);
        }

        $mask = "SELECT id, title FROM feature_tender_raw LIMIT %d, %d";

        if (self::START_ID > 0)  {
            $mask = "SELECT id, title FROM feature_tender_raw WHERE id > "
                . self::START_ID
                . " LIMIT %d, %d";
        }

        for ($i = 1; $i < self::ALL_RECORDS; $i += self::STEP) {
            $sql = sprintf($mask, $i, self::STEP);

            //$this->line($sql);

            $data = DB::select($sql);

            //$this->info(sprintf("%s\t%s", date("H:i:s"), $i));

            $this->writeToFile($data);
        }
    }

    protected function writeToFile($data)
    {
        if (!is_array($data) || sizeof($data) < 1) {
            return;
        }

        $lastId = null;

        foreach ($data as $value) {
            $lastId = $value->id;

            if (!empty($value->title)) {
                file_put_contents($this->filename, $value->title . "\n", FILE_APPEND);
            }
        }

        $this->line($lastId);
    }
}








