<?php
namespace App\Console\Commands\FeatureExtraction;

use App\Component\Ftp\Content\Extractor;
use App\Component\Ftp\Type223\Mapper;
use App\Component\Tools\RegexpTokenizer;
use App\Entities\Ftp\File;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ConvertToRaw extends Command
{
    protected $signature = 'semantic:feature:raw';

    protected $description = 'Data collection';

    protected $tmpDirectory = '/Volumes/disk/raw/';

    /**
     * @var File
     */
    private $fileRepository;

    /**
     * @var RegexpTokenizer
     */
    private $tokenizer;

    public function __construct(File $fileRepository, RegexpTokenizer $tokenizer)
    {
        $this->fileRepository = $fileRepository;

        $this->tokenizer = $tokenizer;

        parent::__construct();
    }

    public function handle()
    {
        $this->info('Start');

        $queryBuilder = $this->fileRepository
            ->getQueryFrom('223')
            ->where('downloaded', 1)
            ->where('unpacked', 0);

        $count = $queryBuilder->count();

        $this->line('Find: ' . $count);

        // нет файлов для чтения
        if ($count < 1) {
            return 0;
        }

        do {
            $file = $queryBuilder->first();

            if ($file !== null) {
                // распаковываем архив
                $this->open($file);

                // ставим отметку о чтение файла
                $file->update(['unpacked' => 1]);
            }
        } while ($file !== null);
    }

    /**
     * @param File $file
     */
    protected function open(File $file)
    {
        $service = new Extractor();
        $list = $service->extract($file, $this->tmpDirectory);

        foreach ($list as $value) {
            $mapper = new Mapper();
            $status = $mapper->read($value);

            if ($status == Mapper::READ_FILE && !$mapper->isEmpty()) {
                $this->think($mapper);
            }

            unlink($value);
        }
    }

    /**
     * @param Mapper $mapper
     */
    protected function think(Mapper $mapper)
    {
        $text = $mapper->getName() . ' ' . $mapper->getDescription();
        $tokens = $this->tokenizer->tokenize($text);

        if ($tokens === null || sizeof($tokens) < 1) {
            return;
        }

        $title = implode(' ', $tokens);
        $hash = md5($title);

        if (empty($title)) {
            return;
        }

        $results = DB::select('SELECT id FROM feature_tender_raw WHERE hash=:hash LIMIT 1;', ['hash' => $hash]);

        if (sizeof($results) > 0) {
            return;
        }

        DB::table('feature_tender_raw')->insert(
            [
                'hash' => $hash,
                'title' => $title
            ]
        );
    }
}








