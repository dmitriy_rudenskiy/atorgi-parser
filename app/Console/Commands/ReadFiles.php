<?php
namespace App\Console\Commands;

use App\Component\Ftp\Content\Extractor;
use App\Component\Ftp\Type223\Mapper;
use App\Component\Semantic\Cluster;
use App\Component\Semantic\Parser;
use App\Component\Semantic\Select;
use Illuminate\Console\Command;
use App\Component\Ftp\Tools\File;
use App\Entities\Ftp\File as Model;
use Illuminate\Support\Facades\Cache;

class ReadFiles extends Command
{
    protected $signature = 'parser:read:files';

    protected $description = 'Read .xml files';

    protected $tmpDirectory = '/Volumes/disk/ftp/tmp/';

    /**
     * @var File
     */
    private $fileRepository;

    /**
     * @var
     */
    private $csvFile;


    public function __construct(Model $fileRepository)
    {
        $this->fileRepository = $fileRepository;

        $this->csvFile = @fopen('/Volumes/disk/result_015.csv', 'w');

        parent::__construct();
    }

    public function handle()
    {
        $queryBuilder = $this->fileRepository
            ->getQueryFrom('223')
            ->where('downloaded', 1)
            ->where('unpacked', 0);

        $count = $queryBuilder->count();

        // нет файлов для чтения
        if ($count < 1) {
            return 0;
        }

        do {
            $file = $queryBuilder->first();

            if ($file !== null) {
                // распаковываем архив
                $this->open($file);

                // ставим отметку о чтение файла
                $file->update(['unpacked' => 1]);
            }
        } while ($file !== null);

        return $count;
    }

    /**
     * @param Model $file
     */
    protected function open(Model $file)
    {
        $service = new Extractor();
        $list = $service->extract($file, $this->tmpDirectory);

        foreach ($list as $value) {
            $mapper = new Mapper();
            $status = $mapper->read($value);

            if ($status == Mapper::READ_FILE && !$mapper->isEmpty()) {
                $this->think($mapper);
            }

            unlink($value);
        }
    }

    /**
     * @param Mapper $mapper
     */
    protected function think(Mapper $mapper)
    {
        $key = md5(sprintf("%s %s 3", $mapper->getName(), $mapper->getNameCompany()));

        // уже добавили такой тендер
        if (Cache::store('file')->get($key) !== null) {
            return;
        }

        $content = sprintf("%s %s", $mapper->getName(), $mapper->getDescription());
        $content = str_replace("\n", ' ', $content);
        $content = str_replace('  ', ' ', $content);

        $type = $this->getType($content);

        if ($type === null) {
            return;
        }

        $data = [
            $type,
            $mapper->getName(),
            $mapper->getDescription(),
            $mapper->getNameCompany()
        ];

        fputcsv($this->csvFile, $data);

        Cache::store('file')->put($key, 1, 720);
    }

    /**
     * @param $text
     * @return string
     */
    protected function getType($text)
    {
        $parser = new Parser($text, false);
        $select = new Select();
        $result = $select->getGroup($parser->get());

        $group = new Cluster($result);
        $category = $group->get();

        return $category;
    }
}








