<?php
namespace App\Console\Commands;

use App\Component\Semantic\Cluster;
use App\Component\Semantic\Parser;
use App\Component\Semantic\Select;
use Illuminate\Console\Command;

class Verifier extends Command
{
    protected $signature = 'semantic:verifier';

    protected $description = 'Checking the quality of texts';

    public function handle()
    {
        $filename = storage_path('tests') . DIRECTORY_SEPARATOR . '2016_12_20_001.csv';

        if (!file_exists($filename)) {
            throw new \InvalidArgumentException();
        }

        if (!is_readable($filename)) {
            throw new \RuntimeException();
        }

        $file = new \SplFileObject($filename);

        while (!$file->eof()) {
            $this->check($file->fgetcsv());
        }
    }

    protected function check(array $data)
    {
        if (sizeof($data) < 2) {
            return false;
        }

        $this->addToFile($data[0], $data[1]);

        return true;
    }

    protected function addToFile($text, $type)
    {
        $parser = new Parser($text);
        $select = new Select();
        $result = $select->getGroup($parser->get());

        $group = new Cluster($result);
        $category = $group->get();

        $item = '"' . $text . '", "';

        if ($type == "1") {
            $item .= "Запчасти отечественных грузовиков";
        } elseif($type == "2") {
            $item .= "Запчасти иностранных грузовиков";
        } else {
            $item .= "?";
        }

        $item .= '", "' . $category .'"' . "\n";

        file_put_contents("/Volumes/disk/result_001.csv", $item, FILE_APPEND);
    }
}








