<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Component\Ftp\Type223\File as File223;
use App\Component\Ftp\Type44\File as File44;

/**
 * Class LoadFromFtp
 * @package App\Console\Commands
 */
class LoadFromFtp extends Command
{
    protected $signature = 'ftp:download:files';

    protected $description = 'Download files from ftp';

    public function handle()
    {
        $this->line('Start load for fz44');
        (new File44())->run();

        $this->line('Start load for fz223');
        (new File223())->run();

        $this->line('Finish work.');
    }
}








