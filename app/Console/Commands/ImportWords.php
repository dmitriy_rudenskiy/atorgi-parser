<?php
namespace App\Console\Commands;

use App\Entities\Catalog\Category;
use App\Entities\Catalog\Word;
use App\Entities\Catalog\WordToCategory;
use Illuminate\Console\Command;

class ImportWords extends Command
{
    protected $signature = 'import:words';

    protected $description = 'Import list words from .csv file';

    /**
     * @var Category
     */
    private $categoryRepository;

    /**
     * @var Word
     */
    private $wordRepository;

    /**
     * @var WordToCategory
     */
    private $wordToCategory;

    public function __construct(Category $categoryRepository, Word $wordRepository, WordToCategory $wordToCategory)
    {
        $this->categoryRepository = $categoryRepository;
        $this->wordRepository = $wordRepository;
        $this->wordToCategory = $wordToCategory;

        parent::__construct();
    }

    public function handle()
    {
        $filename = storage_path('import') . DIRECTORY_SEPARATOR . 'words.csv';

        if (!file_exists($filename)) {
            throw new \InvalidArgumentException();
        }

        if (!is_readable($filename)) {
            throw new \RuntimeException();
        }

        $file = new \SplFileObject($filename);

        while (!$file->eof()) {
            $this->format($file->fgetcsv());
        }
    }

    protected function format(array $data)
    {
        if (sizeof($data) < 11) {
            return false;
        }

        // ,,,daewoo,,1,,,,1,
        list(,,,$lemma,$truck,$car,$buy,$repair,$sparePart,$foreign,$home) = $data;

        $word = $this->wordRepository->get($lemma);

        if (!empty($truck)) {
            $this->wordToCategory->add($word->id, 1);
        }

        if (!empty($car)) {
            $this->wordToCategory->add($word->id, 2);
        }
        if (!empty($buy)) {
            $this->wordToCategory->add($word->id, 3);
        }

        if (!empty($repair)) {
            $this->wordToCategory->add($word->id, 4);
        }

        if (!empty($sparePart)) {
            $this->wordToCategory->add($word->id, 5);
        }

        if (!empty($foreign)) {
            $this->wordToCategory->add($word->id, 6);
        }

        if (!empty($home)) {
            $this->wordToCategory->add($word->id, 7);
        }

        return true;
    }
}








