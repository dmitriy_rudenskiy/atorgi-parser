<?php
namespace App\Console\Commands\Tools;

use App\Component\Semantic\Cluster;
use App\Component\Semantic\Parser;
use App\Component\Semantic\Select;
use Illuminate\Console\Command;

class TestText extends Command
{
    protected $signature = 'semantic:tools:text';

    protected $description = 'Find lemma from text';

    public function handle()
    {
        $text = "Поставка запасных частей для автомобилей Toyota, Камаз";

        $parser = new Parser($text, false);
        $select = new Select();

        echo implode('", "', $parser->get()) . "\n";

        $result = $select->getGroup($parser->get());

        foreach ($result as $key => $value) {
            printf("%d: %d\n", $key, $value);
        }

        $group = new Cluster($result);

        printf("Запчасть: %d\n", (int)$group->isPart());
        printf("Грузовик: %d\n", (int)$group->isTruck());
        printf("Импортный: %d\n", (int)$group->isImport());
        printf("Отечественный: %d\n", (int)$group->isHome());

        $category = $group->get();

        printf("Категория: %s\n", (($category !== null) ? $category : 'не известна'));
    }


}








