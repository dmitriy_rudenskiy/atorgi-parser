<?php
namespace App\Console\Commands\Tools;

use App\Component\Semantic\Parser;
use App\Component\Semantic\Stem;
use App\Entities\Catalog\Word;
use Illuminate\Console\Command;

class FindLemma extends Command
{
    protected $signature = 'semantic:tools:lemma';

    protected $description = 'Find lemma from text';

    public function handle()
    {
        $text = "ПОСТАВКА ДВИГАТЕЛЯ С ТУРБОНАДДУВОМ В ПОЛНОЙ КОМПЛЕКТАЦИИ ДЛЯ АВТОМОБИЛЯ МАЗ- 555102";

        $parser = new Parser($text, false);

        $this->checkList($parser->get());
    }

    protected function checkList(array $data)
    {
        $listWord = Word::whereIn('lemma', $data)->get();

        foreach ($listWord as $value) {
            echo sprintf("%d\t%s\n", $value->id, $value->lemma);

            // ищем не добавленные слова
            foreach ($data as $index => $item ) {
                if ($item == $value->lemma) {
                    unset($data[$index]);
                }
            }

            if ($value->category->count() > 0) {
                foreach ($value->category as $category) {
                    echo sprintf("\t- %s\n", $category->alias);
                }
            } else {
                echo sprintf("EMPTY CATEGORY\n");
            }
        }


        if (sizeof($data) > 0) {
            var_dump($data);
        }
    }

}








