<?php
use App\Entities\Catalog\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'alias' => 'truck',
                'description' => 'Грузовой автомобиль'
            ],
            [
                'id' => 2,
                'alias' => 'car',
                'description' => 'Легковой автомобиль'
            ],
            [
                'id' => 3,
                'alias' => 'buy',
                'description' => 'Покупка'
            ],
            [
                'id' => 4,
                'alias' => 'repair',
                'description' => 'Техническое обслуживание и ремонт автомобиля'
            ],
            [
                'id' => 5,
                'alias' => 'spare_part',
                'description' => 'Детали машин'
            ],
            [
                'id' => 6,
                'alias' => 'foreign',
                'description' => 'Иностранные автомобили'
            ],
            [
                'id' => 7,
                'alias' => 'home',
                'description' => 'Отечественные автомобили'
            ],
            [
                'id' => 8,
                'alias' => 'tire',
                'description' => 'Шины для автомобилей'
            ],
            [
                'id' => 9,
                'alias' => 'remove',
                'description' => 'Удалить тендер'
            ],
        ];


        foreach ($data as $value) {
            Category::forceCreate($value);
        }
    }
}