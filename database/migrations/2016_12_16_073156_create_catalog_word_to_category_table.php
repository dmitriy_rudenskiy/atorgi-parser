<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogWordToCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalog_word_to_category', function (Blueprint $table) {
            $table->integer('word_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('grade');

            $table->primary(['word_id', 'category_id']);
            $table->foreign('word_id')->references('id')->on('catalog_word');
            $table->foreign('category_id')->references('id')->on('catalog_category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('catalog_word_to_category');
    }
}
