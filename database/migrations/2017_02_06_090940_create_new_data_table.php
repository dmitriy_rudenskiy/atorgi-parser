<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateNewDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::insert("INSERT catalog_word_to_category (word_id, category_id, grade) VALUES (154, 1, 10);");
        DB::insert("INSERT catalog_word_to_category (word_id, category_id, grade) VALUES (154, 7, 10);");
        DB::insert("INSERT catalog_word_to_category (word_id, category_id, grade) VALUES (556, 1, 10);");
        DB::insert("INSERT catalog_word_to_category (word_id, category_id, grade) VALUES (556, 7, 10);");

        DB::insert("INSERT INTO catalog_word (id, is_active, lemma) VALUES (801, 1, 'xcmg');");
        DB::insert("INSERT INTO catalog_word (id, is_active, lemma) VALUES (802, 1, 'l34');");
        DB::insert("INSERT INTO catalog_word (id, is_active, lemma) VALUES (803, 1, 'готтвальд');");
        DB::insert("INSERT INTO catalog_word (id, is_active, lemma) VALUES (804, 1, 'кран');");
        DB::insert("INSERT INTO catalog_word (id, is_active, lemma) VALUES (805, 1, 'сменнозапасной');");
        DB::insert("INSERT INTO catalog_word (id, is_active, lemma) VALUES (806, 1, 'кондор');");
        DB::insert("INSERT INTO catalog_word (id, is_active, lemma) VALUES (807, 1, 'альбатрос');");
        DB::insert("INSERT INTO catalog_word (id, is_active, lemma) VALUES (808, 1, 'сокол');");
        DB::insert("INSERT INTO catalog_word (id, is_active, lemma) VALUES (809, 1, 'зч');");
        DB::insert("INSERT INTO catalog_word (id, is_active, lemma) VALUES (810, 1, 'отечественый');");
        DB::insert("INSERT INTO catalog_word (id, is_active, lemma) VALUES (811, 1, 'тракторной');");
        DB::insert("INSERT INTO catalog_word (id, is_active, lemma) VALUES (812, 1, 'дорожностроительный');");

        DB::insert("INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (801, 1, 10);");
        DB::insert("INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (801, 6, 10);");
        DB::insert("INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (802, 1, 10);");
        DB::insert("INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (802, 6, 10);");
        DB::insert("INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (804, 1, 10);");
        DB::insert("INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (805, 5, 10);");
        DB::insert("INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (806, 7, 10);");
        DB::insert("INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (807, 7, 10);");
        DB::insert("INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (808, 7, 10);");
        DB::insert("INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (809, 5, 10);");
        DB::insert("INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (810, 7, 10);");
        DB::insert("INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (811, 7, 10);");
        DB::insert("INSERT INTO catalog_word_to_category (word_id, category_id, grade) VALUES (812, 1, 10);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
