<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateNewDataTwoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::insert("UPDATE catalog_word_to_category  SET grade=100 WHERE word_id=70 AND category_id=1;");
        DB::insert("UPDATE catalog_word_to_category  SET grade=100 WHERE word_id=70 AND category_id=7;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
