<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFtpFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ftp_file', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('downloaded')->default(false);
            $table->boolean('unpacked')->default(false);
            $table->integer('path_id');
            $table->integer('region_id');
            $table->string('hash', 32);
            $table->string('url', 255);
            $table->timestamps();

            $table->index('downloaded');
            $table->index('unpacked');
            $table->unique('hash');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ftp_file');
    }
}
